package com.gildedrose;

import org.approvaltests.combinations.CombinationApprovals;

import org.junit.jupiter.api.Test;

class GildedRoseTest {

    @Test
    void updateQualityForItems() {
        String[] names = {"sword", "Aged Brie", "Sulfuras, Hand of Ragnaros", "Backstage passes to a TAFKAL80ETC concert"};
        Integer[] sellIns = {-1, 0, 1, 5, 6, 7, 10, 11, 12};
        Integer[] qualities = {-1, 0, 1, 48, 49, 50, 51};

        CombinationApprovals.verifyAllCombinations(this::doUpdateQuality, names, sellIns, qualities);
    }

    private String doUpdateQuality(String name, int sellIn, int quality) {
        Item[] items = new Item[] { new Item(name, sellIn, quality) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();

        String item = app.items[0].toString();
        return item;
    }

}
